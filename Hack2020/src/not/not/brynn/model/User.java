package not.not.brynn.model;


public class User {


	@com.google.gson.annotations.SerializedName("firstname")
	private String mFirstName;

	
	@com.google.gson.annotations.SerializedName("id")
	private String mId;

	
	@com.google.gson.annotations.SerializedName("lastname")
	private String mLastName;
	
	public String getmLastName() {
		return mLastName;
	}

	@com.google.gson.annotations.SerializedName("login")
	private String mLogin;
	
	public String getmLogin() {
		return mLogin;
	}

	@com.google.gson.annotations.SerializedName("password")
	private String mPassword;
	

	public String getmPassword() {
		return mPassword;
	}

	public void setmPassword(String mPassword) {
		this.mPassword = mPassword;
	}

	public String getmFirstName() {
		return mFirstName;
	}

	public void setmFirstName(String mFirstName) {
		this.mFirstName = mFirstName;
	}

	public String getmId() {
		return mId;
	}

	public void setmId(String mId) {
		this.mId = mId;
	}

	public String ismLastName() {
		return mLastName;
	}

	public void setmLastName(String mLastName) {
		this.mLastName = mLastName;
	}

	public String ismLogin() {
		return mLogin;
	}

	public void setmLogin(String mLogin) {
		this.mLogin = mLogin;
	}

	public User() {

	}

	@Override
	public String toString() {
		return getmFirstName();
	}


	public User(String id, String firstname, String lastname,String login) {
		this.setmFirstName(firstname);
		this.setId(id);
		this.setmLastName(lastname);
		this.setmLogin(login);
	}

	public String getId() {
		return mId;
	}
	public final void setId(String id) {
		mId = id;
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof ToDoItem && ((User) o).mId == mId;
	}
}
